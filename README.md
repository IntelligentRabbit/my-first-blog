# Intelligent Rabbit Blog :rabbit:
[It's a simple blog](http://intelligentrabbit.pythonanywhere.com/) created as a practical work following the [manual](https://tutorial.djangogirls.org/uk/)

# Prerequisites
Project used Django and Whitenoise.
Project dependiencies are listed in `requirements.txt`.
